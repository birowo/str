package str

var x = [...]int{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 3, 4}

type Str struct {
	Str      []byte
	Bgn, End int
}

func (s *Str) ChrPos() (ok bool) {
	ok = s.End < len(s.Str)
	if ok {
		s.Bgn = s.End
		s.End += x[(s.Str[s.End]&0b11110000)>>4]
	}
	return
}
