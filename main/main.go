package main

import (
	"math/rand"
	"time"

	"gitlab.com/birowo/str"
)

func Shuffle(rnd *rand.Rand, n int32, swap func(int32, int32)) {
	for n > 1 {
		n--
		swap(n, rnd.Int31n(n))
	}
}

func BsFill(dst, fill []byte) (l int) {
	dstLen, fillLen := len(dst)+1, len(fill)
	for (l + fillLen) < dstLen {
		copy(dst[l:], fill)
		l += fillLen
	}
	return
}

type Pos struct {
	Bgn, End int
}

func ChrsPos2Str(chrsPos []Pos, bs []byte) (ret []byte) {
	retLen := len(bs)
	ret = make([]byte, retLen)
	l, i := 0, 0
	for l < retLen {
		l += copy(ret[l:], bs[chrsPos[i].Bgn:chrsPos[i].End])
		i++
	}
	return
}
func main() {
	strLen := 1020
	bs := make([]byte, strLen)
	fill := []byte("$Φ界😂")
	bs = bs[:BsFill(bs, fill)]
	chrsPos := make([]Pos, 0, len(bs))
	str := &str.Str{Str: bs}
	for str.ChrPos() {
		chrsPos = append(chrsPos, Pos{str.Bgn, str.End})
	}
	runes := []rune(string(bs))
	Shuffle(
		rand.New(rand.NewSource(time.Now().Unix())),
		int32(strLen/len(fill)*4),
		func(i, j int32) {
			chrsPos[i], chrsPos[j] = chrsPos[j], chrsPos[i]
			runes[i], runes[j] = runes[j], runes[i]
		},
	)
	str1 := string(ChrsPos2Str(chrsPos, bs))
	str2 := string(runes)
	println("STR:", str1)
	println("RNS:", str2)
	println(str1 == str2)
}
